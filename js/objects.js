render = function(className, gameDiv) {
    var exist = document.getElementById(this.id),
        b = exist ? exist: document.createElement('b');

    b.id = this.id;
    b.className = className;

    b = gameDiv.appendChild(b);

    $(b).css({
        'top': (100 + this.i *16) +'px',
        'left': (10+ this.j *16) +'px'
    });

    return b;
}

var moveDiff = 0.25;

function Space(i, j, objects) {
    this.objects = objects;
    this.id = Math.random();
    this.i = i;
    this.j = j;
    this.isPassable = true;

    this.render = function(div) {
        render.call(this, 'space', div);
    }
    }


function Stone() {
    Space.apply(this, arguments);
    this.isPassable = false;
    
    this.render = function(div) {
        this.render = function() {} // rendered
        return render.call(this, 'stone', div)
    }
}


function Brick() {
    Space.apply(this, arguments);
    this.isToggle = true;
    this.isPassable = false;
    
    this.render = function(div) {
        if (!this.isToggle) { return }

        return render.call(this, 'brick', div)
    }
}


function Ladder() {
    Space.apply(this, arguments);
    
    this.render = function(div) {
        this.render = function() {} // rendered

        return render.call(this, 'ladder', div)
    }
}

function FinishLadder() {
    Space.apply(this, arguments);
    this.render = function(div) {
        this.render = function() {} // rendered

        return render.call(this, 'ladder', div)
    }// fixme
}

function FinishBrick() {
    Space.apply(this, arguments);
    this.render = function(div) {
        this.render = function() {} // rendered
        return render.call(this, 'brick', div)
    }// fixme
}

function Rope() {
    Space.apply(this, arguments);
    
    this.render = function(div) {
        this.render = function() {} // rendered

        return render.call(this, 'rope', div)
    }
}


function Gold() {
    Space.apply(this, arguments);
    
    this.render = function(div) {
        if (this.isCollected) {
            this.render = function() {} // rendered
            return null
        }
        return render.call(this, 'gold', div)
    }
}


function Person(i, j) {
    Space.apply(this, arguments);
    this.canMove = true;
    this.v = [0, 0];
    this.i = i;
    this.j = j;
    this.runLeft = function() {
        this.v[1] = -1;
    };
    this.stopRunLeft = function() {
        this.v[1] = 0;
    };
    this.runRight = function() {
        this.v[1] = 1;
    };
    this.stopRunRight = function() {
        this.v[1] = 0;
    };
    this.goUp = function() {
        this.v[0] = -1;
    };
    this.stopGoUp = function() {
        this.v[0] = 0;
    };
    this.goDown = function() {
        this.v[0] = 1;
    };
    this.stopGoDown = function() {
        this.v[0] = 0;
    };
    this.getObject = function(i, j) {
        for (var k = 0; k < this.objects.level.length; k += 1) {
            var level = this.objects.level[k];
            if (level.i == i && level.j == j) {
                return level;
}
        }
    };
    this.getEnvironment = function() {
        return this.getObject(Math.floor(this.i + 0.5), Math.floor(this.j + 0.5));
    };
    this.getLeftBlock = function() {
        return this.getObject(Math.floor(this.i + 0.5), Math.floor(this.j + 0.5) - 1);
    };
    this.getRightBlock = function() {
        return this.getObject(Math.floor(this.i+ 0.5), Math.floor(this.j + 0.5) + 1);
    };
    this.getGround = function() {
        return this.getObject(Math.floor(this.i + 0.5) + 1, Math.floor(this.j + 0.5));
    };
    this.isInAir = function() {
        var environ = this.getEnvironment();
        var iDiff = this.i - environ.i;
        return (environ instanceof Space && iDiff < 0) ||
               (this.getGround() instanceof Space && !(environ instanceof Rope || environ instanceof Ladder));
    };
    this.canMove = function() {
        /*
         При выдаче результата должен учитывать направление движения.
         */
        var moveV = [0, 0];
        var environment = this.getEnvironment();
        var ground = this.getGround();
        var yDiff = this.i - environment.i;
        var xDiff = this.j - environment.j;
        if (environment instanceof Ladder || ground instanceof Ladder) {
            moveV[0] = 1;
            moveV[1] = 1;
        } else if (environment instanceof Rope || ground instanceof Brick || ground instanceof Stone || ground instanceof Ladder) {
            moveV[1] = 1;
        }
        if (environment instanceof Space && yDiff != 0) {
            moveV[1] = 0;
        }
        // Краевые условия
        if (!this.getLeftBlock().isPassable && xDiff <= 0 && this.v[1] == -1) {
            // При движении влево
            // Если слева непроходимый блок
            moveV[1] = 0;
        } else if (!this.getRightBlock().isPassable && xDiff >= 0 && this.v[1] == 1) {
            // При движении вправо
            // Если справа непроходимый блок
            moveV[1] = 0;
        }
        if (!this.getGround().isPassable && yDiff >= 0 && this.v[0] == 1) {
            // При движении вниз
            // Если под землей непроходимый блок и персонаж припущен в землю
            moveV[0] = 0;
        } else if (ground instanceof Ladder && environment instanceof Space && yDiff <= 0 && this.v[0] == -1) {
            // При движении вверх
            // Находится в воздухе, стоит на лестнице и подвешен в воздухе
            moveV[0] = 0;
        }
        document.getElementById('debug-y-diff').innerHTML = yDiff;
        document.getElementById('debug-x-diff').innerHTML = xDiff;
        return moveV;
    };
    this.move = function() {
        if (this.isInAir()) {
            this.i += moveDiff;
        } else {
            var canMoveV = this.canMove();
            this.i += moveDiff * this.v[0] * canMoveV[0];
            this.j += moveDiff * this.v[1] * canMoveV[1];
        }
        document.getElementById('debug-vector').innerHTML = canMoveV.toString() + '; ' + this.v.toString();
    }
}


function Player() {
    Person.apply(this, arguments);
    this.digLeft = function() {

    };
    this.digRight = function() {

    };

    this.render = function(div) {
        return render.call(this, 'player', div)
    }
}


function Enemy() {
    Person.apply(this, arguments);
    
    this.render = function(div) {
        return render.call(this, 'enemy', div)
    }
}
