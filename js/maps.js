window.levels = [
[
    '    $             h         ',
    'XXXXXXXHXXXXXXX   h         ',
    '       H~~~~~~~~~~h    $    ',
    '       H    XXH   XXXXXXXHXX',
    '     - H    XXH       $- H  ',
    'XXHXXXXX    XXXXXXXXHXXXXXXX',
    '  H           -     H       ',
    'XXXXXXXXXHXXXXXXXXXXH       ',
    '         H          H       ',
    '       $ H~~~~~~~~~~H   $   ',
    '    HXXXXXX         XXXXXXXH',
    '    H     +      $         H',
    'XXXXXXXXXXXXXXXXXXXXXXXXXXXX'
],[
    '   $              $        H',
    'H**X**H    HXXXXXXXXXH $   H',
    'H $ - H    H         HXXXXVH',
    'HX*X*XH    H         H     h',
    'H     H~~~~H~~~~~~  -H     h',
    'H     H    H  $  HXXX******H',
    'H   - H $  HXXXXXH         H',
    '*XXX*XX*XX*H         HXXXHXX',
    '*XXX*      H         H   H  ',
    '*$  *      H   ~~~~~~H   H $',
    'XXXXXXXXHXXX****     H  XXXX',
    '        H +          H      ',
    'XXXXXXXXXXXXXXXXXXXXXXXXXXXX'
],[
    '~~~~~~~~~~    $            h',
    'H $      HXXXXXXXXXXH      h',
    'XXXXXH - H     $    HX******',
    '     HXXXXXXHXXXXXHXX       ',
    '  $  H      H     H  ~~     ',
    'XXXXHX      H  -  H    ~~   ',
    '    H    HXXXXXXHXX      ~~$',
    '    H~~~~H      H  -       X',
    '    H       HXXXXXXXXXH     ',
    'XXXHXXXXXXXXXX   $   XXXXXHX',
    'XXXHXXXXXXXXXX HXXXH XXXXXHX',
    '   H    +      HXXXH   $  H ',
    'XXXXXXXXXXXXXXXXXXXXXXXXXXXX'
]];

//href = http://www.gamefaqs.com/nes/587420-lode-runner/faqs/40192

var objectMap = {
    '*': Stone,
    'X': Brick,
    'H': Ladder,
    '~': Rope,
    '$': Gold,
    '-': Enemy,
    '+': Player,
    ' ': Space,
    'h': FinishLadder,
    'V': FinishBrick
};


function parseMap(levelNum) {
    var rawLevel = window.levels[levelNum];
    var level = [];
    var persons = [];
    var objects = {level: level, persons: persons};
    for(var i = 0; i < rawLevel.length; i += 1) {
        var row = rawLevel[i];
        level.push(new Stone(i, 0, objects));
        level.push(new Stone(i, row.length + 1, objects));
        for(var j = 1; j <= row.length; j += 1) {
            var object = new objectMap[row[j-1]](i, j, objects);
            if (object.canMove) {
                level.push(new Space(i, j, objects));
                persons.push(object)
            } else {
                level.push(object)
            }
        }
    }
    for(var j = 0; j <= rawLevel[0].length + 1; j += 1) {
        level.push(new Stone(rawLevel.length, j, objects));
    }
    return objects;
}
