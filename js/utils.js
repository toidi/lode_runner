function getPlayer(map) {
    for (var i = 0; i < map.persons.length; i += 1) {
        var person = map.persons[i];
        if (person instanceof Player) {
            return person
        }
    }
}