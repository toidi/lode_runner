window.onload = function() {
(function initEngine(w, settings) {
    var isRendered = false;

    var gameDiv = document.getElementById(settings.divId);

    var timeout = 100;
    var timeouthandler;

    var currentLevel = 0;
    var map = parseMap(currentLevel);

    var player = getPlayer(map);

    /*
     * key codes:
     *  +  37 - left arrow
     *  +  38 - up arrow
     *  +  39 - right arrow
     *  +  40 - down arrow
     *  +  27 - escape
     *  +  32 - space
     *  +  81 - 'q'
     *  +  87 - 'w'
     */
    function initListeners() {
        w.addEventListener('keydown', function(e) {
            switch (e.keyCode) {
                case 37:
                    player.runLeft();
                    break;
                case 38:
                    player.goUp();
                    break;
                case 39:
                    player.runRight();
                    break;
                case 40:
                    player.goDown();
                    break;
                case 81:
                    player.digLeft();
                    break;
                case 87:
                    player.digRight();
                    break;
            }
        });
        w.addEventListener('keyup', function(e) {
            switch (e.keyCode) {
                case 37:
                    player.stopRunLeft();
                    break;
                case 38:
                    player.stopGoUp();
                    break;
                case 39:
                    player.stopRunRight();
                    break;
                case 40:
                    player.stopGoDown();
                    break
            }
        });
    }

    function restart() {

    }

    function pause() {

    }


    function debug() {
        var debugElem = document.getElementById('debug-coords');
        var debugEnvironment = document.getElementById('debug-environment');
        var debugGround = document.getElementById('debug-ground');
        debugElem.innerHTML = [player.i, player.j].toString();
        debugEnvironment.innerHTML = player.getEnvironment().constructor.name;
        debugGround.innerHTML = player.getGround().constructor.name;
        document.getElementById('debug-right').innerHTML = player.getRightBlock().constructor.name;
        document.getElementById('debug-left').innerHTML = player.getLeftBlock().constructor.name;
    }


    function update() {
        for (var i = 0; i < map.persons.length; i += 1) {
            var person = map.persons[i];
            person.move();
        }
    }


    function render() {
        if (!isRendered) {
            map.level.forEach(function(item) {
                item.render.call(item, gameDiv)
            });
        }
        map.persons.forEach(function(item) {
            item.render.call(item, gameDiv)
        });

        isRendered = true
    }

    function tick() {
        update();
        render();
        debug();
    }

    initListeners();
    timeouthandler = setInterval(tick, timeout);
})(window, {'divId': 'game'});
}
