Сигнатуры объектов
==================

Block(abstract)
-----

Функции:

 +  update

 +  isSticky

 +  isFull

 +  isPassable

 +  render

 +  dig


Space
-----


Brick
-----

Функции:

 +  dig


Stone
-----


Gold
----

 +  collect


Ladder
------


Rope
----


Player
------

Функции:

 +  render

 +  runLeft

 +  stopRunLeft

 +  runRight

 +  stopRunRight

 +  goUp

 +  stopGoUp

 +  goDown

 +  stopGoDown

 +  digBrick

 +  collectGold

 +  die

 +  digLeft

 +  digRight


Enemy
-----

Функции:

 +  getPath

 +  catchPerson
